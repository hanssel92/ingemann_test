﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ingemann_test.Models
{
    public class Factura
    {


        [Column(TypeName = "int", Order = 1),
       DatabaseGenerated(DatabaseGeneratedOption.Identity),Key]
        public int FacturaId { get; set; }

        [Required]
        public string? FacturaCodigo { get; set; }
        public DateTime Fecha { get; set; }

        public float TotalPagar { get; set; }


        //llaves foreanas ************************************************************************************
        public short MonedaId { get; set; }
        public virtual Moneda? Monedas { get; set; }

        public short ImpuestoId { get; set; }
        public virtual Impuesto? Impuestos { get; set; }


        public DateTime FechaRegistro { get; set; }
        //************************************************************************************ llaves foreanas 



    }
}
