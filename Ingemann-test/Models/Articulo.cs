﻿#nullable disable
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ingemann_test.Models
{
    public class Articulo
    {
        [Column(TypeName = "int", Order = 1),
         DatabaseGenerated(DatabaseGeneratedOption.Identity),
         Key]
        public int ArticuloId { get; set; }

        public string Descripcion { get; set; }
        public float Precio { get; set; }
        public float Costo { get; set; }

        public bool Activo { get; set; }


    }
}
