﻿#nullable disable
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ingemann_test.Models
{
    public class FacturaDetalle
    {
        [Column(TypeName = "int", Order = 1),
    DatabaseGenerated(DatabaseGeneratedOption.Identity),Key]
        public int FacturaDetalleId { get; set; }

        public int FacturaId { get; set; }
        public virtual Factura Facturas { get; set; }

        public int ArticuloId { get; set; }
        public virtual Articulo Articulos { get; set; }

        public short Cantidad { get; set; }

        public float Subtotal { get; set; }

        public short ImpuestoId { get; set; }

        public float ValorImpuesto { get; set; }

        [NotMapped]
        public float Total { get; set; }



    }
}
