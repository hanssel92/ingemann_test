﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ingemann_test.Models
{
    public class Impuesto
    {

        [Column(TypeName = "smallint", Order = 1),
    DatabaseGenerated(DatabaseGeneratedOption.Identity),
    Key]
        public short ImpuestoId { get; set; }
        public decimal Porcentaje { get; set; }
    }
}
