﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ingemann_test.Models
{
    public class Moneda
    {

        [Column(TypeName = "smallint", Order = 1),
           DatabaseGenerated(DatabaseGeneratedOption.Identity),
           Key]
        public short MonedaId { get; set; }
        public string? Nombre { get; set; }

        public string? Signo { get; set; }
    }
}
