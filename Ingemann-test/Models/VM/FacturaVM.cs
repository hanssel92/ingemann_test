﻿#nullable disable 
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ingemann_test.Models.VM
{
    public class FacturaVM
    {
        [Key]
        public int FacturaId { get; set; }

        [Required]
        public string FacturaCodigo { get; set; }
        public DateTime Fecha { get; set; }

        public float TotalPagar { get; set; }


        public short MonedaId { get; set; }
        public virtual Moneda Monedas { get; set; }

        public short ImpuestoId { get; set; }
        public virtual Impuesto Impuestos { get; set; }


     
        public List<FacturaDetalle> FacturaDetalles { get; set; }
    }
}
