﻿#nullable disable
using Microsoft.EntityFrameworkCore;
using Ingemann_test.Models;
using Ingemann_test.Models.VM;

namespace Ingemann_test.Data
{
    public class IngemannTestContext: DbContext
    {

        public IngemannTestContext(DbContextOptions<IngemannTestContext> options) : base(options)
        {
        }

        public DbSet<Articulo> Articulo { get; set; }

        public DbSet<Moneda> Moneda { get; set; }
        public DbSet<Impuesto> Impuesto { get; set; }

        public DbSet<Factura> Factura { get; set; }

        public DbSet<FacturaDetalle> FacturaDetalle { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Factura>()
                .Property(b => b.Fecha)
                .HasDefaultValueSql("getdate()");

            modelBuilder.Entity<Factura>()
            .Property(b => b.FechaRegistro)
            .HasDefaultValueSql("getdate()");
        }



    }
}
