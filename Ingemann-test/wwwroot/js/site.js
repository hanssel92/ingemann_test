﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.
let contadorhab = 0;
let sumaSubtotal = 0;
let sumaValorimpuesto = 0;
let sumaTotal =0;


// Write your JavaScript code.
$("#AgregarArticulo").click(function AgregarArticulo() {

    let idarticulo = $("#ddlarticulo :selected").val();
    let nombrearticulo = $("#ddlarticulo :selected").text();
    let cantidaarticulo = $("#cantidadArticulo").val();
    let precioarticulo = 1;
    let subtotal = 0.00;
    let impuestovalor = parseFloat($("#ImpuestoId :selected").text()) / 100;
    let valorimpuesto = 0.00
    let total = 0.00;



    $.ajax({
        type: 'GET',
        url: getArticuloInfo, // we are calling json method
        dataType: 'json',
        data: {
            id: idarticulo
        },
        success: function (r) {
            precioarticulo = r.articulo.precio;
            if (idarticulo) {
                contadorhab += 1;
                subtotal = cantidaarticulo * precioarticulo;
                valorimpuesto = subtotal * impuestovalor;
                total = subtotal + valorimpuesto;
                $("tbody.articulotable").append(
                    "<tr class=success>"
                    + "<td><input readonly class='form-control'  style='width: 60px' type='text' name='FacturaDetalles[" + parseInt(contadorhab) + "].ArticuloId' value='" + parseInt(idarticulo) + "'> </td>"
                    + "<td><input readonly class='form-control'  style='pointer-events: none;'  type='text' name='FacturaDetalles[" + parseInt(contadorhab) + "].Descripcion' value='" + nombrearticulo + "'> </td>"
                    + "<td><input readonly class='form-control'  style='width: 100px' id='articuloprecio-" + parseInt(contadorhab) + "' name='FacturaDetalles[" + parseInt(contadorhab) + "].Articulos.Precio' value='" + parseFloat(precioarticulo) + "'> </td>"
                    + "<td><input readonly class='form-control'  style='width: 100px'  id='articulocantidad-" + parseInt(contadorhab) + "'  required name='FacturaDetalles[" + parseInt(contadorhab) + "].Cantidad'  value='" + parseInt(cantidaarticulo) + "'> </td>"
                    + "<td><input readonly class='form-control' style='width: 100px'type='text' id='articuloSubtotal-" + parseInt(contadorhab) + "' name='FacturaDetalles[" + parseInt(contadorhab) + "].Subtotal' value='" + parseFloat(subtotal) + "'> </td>"
                    + "<td><input readonly class='form-control' style='width: 100px'type='text' id='articuloValorImpuesto-" + parseInt(contadorhab) + "' name='FacturaDetalles[" + parseInt(contadorhab) + "].ValorImpuesto' value='" + valorimpuesto + "'> </td>"
                    + "<td><input readonly class='form-control' style='width: 100px'type='text' id='articuloTotal-" + parseInt(contadorhab) + "' name='FacturaDetalles[" + parseInt(contadorhab) + "].Total' value='" + parseFloat(total) + "'> </td>"
                    + "<td><input  type='button' id='quitarArticulo' value='Quitar Articulo' class='btn btn-danger'></td>"
                    + "</tr>"
                );
            }
        }
    });

    //ActualizaResumen();

    //para quitar el item de la tabla
    $('#articulotb').on('click', 'input[value="Quitar Articulo"]', function () {
        $(this).closest('tr').remove();
    });

    //se actualiza 

});








//function ActualizaResumen() {
////Ejecuto la función al cargar la página
//    $(document).ready(function () {
//        var total_col4 = 0;
//        var total_col5 = 0;
//        var total_col6 = 0;

//        var asd = $("#articuloSubtotal-1").val();

//        console.log(asd);

//        var nFilas = $("#articulotb tr").length;
//        console.log(nFilas);

//        for (i = 0; i <= nFilas; i++) {
//            var text = "#articuloSubtotal-" + parseInt(i) + "";
//            total_col4 += parseFloat($(text).val());

//            total_col5 += $("#articuloValorImpuesto-" + parseInt(i) + "").val();
//            total_col6 += $("#articuloTotal-" + parseInt(i) + "").val();
//            console.log(text)

//        }



//        console.log(total_col5)
//        console.log(total_col6)

   
//    //$("#resumensubtotallbl").html(total_col4);
//    //$("#resumenIVAlbl").html(total_col5);
//    //$("#resumenTotallbl").html(total_col6);
//  //Defino los totales de mis 2 columnas en 0
  

//});
//};

