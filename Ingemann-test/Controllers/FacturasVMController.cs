﻿#nullable disable
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Ingemann_test.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Ingemann_test.Models;
using Ingemann_test.Models.VM;

namespace Ingemann_test.Controllers
{
    public class FacturasVMController : Controller
    {
        private readonly IngemannTestContext _context;

        public FacturasVMController(IngemannTestContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            
            var ingemannTestContext = _context.Factura.Include(f => f.Impuestos).Include(f => f.Monedas);
            return View(await ingemannTestContext.ToListAsync());
        }

        // GET: FacturasVMController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: FacturasVMController/Create
        public ActionResult Create()
        {
            var numero = _context.Factura.Max(u => (int?)u.FacturaId) + 1;
            DateTime fecha = DateTime.Now;
            ViewBag.Codigo = $"FA-{numero}";
           
            ViewBag.MonedaId = new SelectList(_context.Moneda, "MonedaId", "Nombre");
            ViewBag.ImpuestoId = new SelectList(_context.Impuesto, "ImpuestoId", "Porcentaje");

            ViewBag.ArticuloId = new SelectList(_context.Articulo, "ArticuloId", "Descripcion");

            return View();
        }

        // POST: FacturasVMController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FacturaVM facturaVM)
        {
          
                var numero = _context.Factura.Max(u => (int?)u.FacturaId) + 1;

            Factura factura = new Factura();
                List<FacturaDetalle> facturaDetalle = new List<FacturaDetalle>();

                factura.FacturaCodigo = $"FA-{numero}";
                factura.FechaRegistro = DateTime.Now;
                factura.MonedaId = facturaVM.MonedaId;
                factura.ImpuestoId = facturaVM.ImpuestoId;
                factura.TotalPagar = facturaVM.FacturaDetalles.Sum(e => e.Total);

                _context.Add(factura);

            await _context.SaveChangesAsync();


            foreach (var item in facturaVM.FacturaDetalles)
                {
                    if (item.ArticuloId != 0)
                    {
                        facturaDetalle.Add(new FacturaDetalle { 
                        FacturaId = factura.FacturaId
                        , ArticuloId = item.ArticuloId
                        , Cantidad = item.Cantidad
                        , Subtotal = item.Subtotal
                        , ImpuestoId = item.ImpuestoId
                        , ValorImpuesto = item.ValorImpuesto
                        , Total = item.Total
                    });
                    }
                        
                }

                _context.AddRange(facturaDetalle);
            await _context.SaveChangesAsync();

            return View();
        }

        // GET: FacturasVMController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: FacturasVMController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FacturasVMController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: FacturasVMController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }


        public async Task<IActionResult> getArticuloInfo(int id)
        {
            //return Json(new { turno = turno.Select(d => new { d.Id, d.Nombre }) });


            var articulo = await _context.Articulo.FirstOrDefaultAsync(m => m.ArticuloId == id);
            return Json(new { articulo });
        }
    }
}
