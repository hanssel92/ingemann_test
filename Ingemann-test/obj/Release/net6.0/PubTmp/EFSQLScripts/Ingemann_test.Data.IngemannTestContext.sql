﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE TABLE [Articulo] (
        [ArticuloId] int NOT NULL IDENTITY,
        [Descripcion] nvarchar(max) NULL,
        [Precio] real NOT NULL,
        [Costo] real NOT NULL,
        [Activo] bit NOT NULL,
        CONSTRAINT [PK_Articulo] PRIMARY KEY ([ArticuloId])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE TABLE [Impuesto] (
        [ImpuestoId] smallint NOT NULL IDENTITY,
        [Porcentaje] decimal(18,2) NOT NULL,
        CONSTRAINT [PK_Impuesto] PRIMARY KEY ([ImpuestoId])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE TABLE [Moneda] (
        [MonedaId] smallint NOT NULL IDENTITY,
        [Nombre] nvarchar(max) NULL,
        [Signo] nvarchar(max) NULL,
        CONSTRAINT [PK_Moneda] PRIMARY KEY ([MonedaId])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE TABLE [Factura] (
        [FacturaId] int NOT NULL IDENTITY,
        [FacturaCodigo] nvarchar(max) NOT NULL,
        [Fecha] datetime2 NOT NULL DEFAULT (getdate()),
        [TotalPagar] real NOT NULL,
        [MonedaId] smallint NOT NULL,
        [ImpuestoId] smallint NOT NULL,
        [FechaRegistro] datetime2 NOT NULL DEFAULT (getdate()),
        CONSTRAINT [PK_Factura] PRIMARY KEY ([FacturaId]),
        CONSTRAINT [FK_Factura_Impuesto_ImpuestoId] FOREIGN KEY ([ImpuestoId]) REFERENCES [Impuesto] ([ImpuestoId]) ON DELETE CASCADE,
        CONSTRAINT [FK_Factura_Moneda_MonedaId] FOREIGN KEY ([MonedaId]) REFERENCES [Moneda] ([MonedaId]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE TABLE [FacturaVM] (
        [FacturaId] int NOT NULL IDENTITY,
        [FacturaCodigo] nvarchar(max) NOT NULL,
        [Fecha] datetime2 NOT NULL,
        [TotalPagar] real NOT NULL,
        [MonedaId] smallint NOT NULL,
        [ImpuestoId] smallint NOT NULL,
        CONSTRAINT [PK_FacturaVM] PRIMARY KEY ([FacturaId]),
        CONSTRAINT [FK_FacturaVM_Impuesto_ImpuestoId] FOREIGN KEY ([ImpuestoId]) REFERENCES [Impuesto] ([ImpuestoId]) ON DELETE CASCADE,
        CONSTRAINT [FK_FacturaVM_Moneda_MonedaId] FOREIGN KEY ([MonedaId]) REFERENCES [Moneda] ([MonedaId]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE TABLE [FacturaDetalle] (
        [FacturaDetalleId] int NOT NULL IDENTITY,
        [FacturaId] int NOT NULL,
        [ArticuloId] int NOT NULL,
        [Cantidad] smallint NOT NULL,
        [Subtotal] real NOT NULL,
        [FacturaVMFacturaId] int NULL,
        CONSTRAINT [PK_FacturaDetalle] PRIMARY KEY ([FacturaDetalleId]),
        CONSTRAINT [FK_FacturaDetalle_Articulo_ArticuloId] FOREIGN KEY ([ArticuloId]) REFERENCES [Articulo] ([ArticuloId]) ON DELETE CASCADE,
        CONSTRAINT [FK_FacturaDetalle_Factura_FacturaId] FOREIGN KEY ([FacturaId]) REFERENCES [Factura] ([FacturaId]) ON DELETE CASCADE,
        CONSTRAINT [FK_FacturaDetalle_FacturaVM_FacturaVMFacturaId] FOREIGN KEY ([FacturaVMFacturaId]) REFERENCES [FacturaVM] ([FacturaId])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE INDEX [IX_Factura_ImpuestoId] ON [Factura] ([ImpuestoId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE INDEX [IX_Factura_MonedaId] ON [Factura] ([MonedaId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE INDEX [IX_FacturaDetalle_ArticuloId] ON [FacturaDetalle] ([ArticuloId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE INDEX [IX_FacturaDetalle_FacturaId] ON [FacturaDetalle] ([FacturaId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE INDEX [IX_FacturaDetalle_FacturaVMFacturaId] ON [FacturaDetalle] ([FacturaVMFacturaId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE INDEX [IX_FacturaVM_ImpuestoId] ON [FacturaVM] ([ImpuestoId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    CREATE INDEX [IX_FacturaVM_MonedaId] ON [FacturaVM] ([MonedaId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315142931_MyFirstMigration')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20220315142931_MyFirstMigration', N'6.0.3');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315184449_camposdetalle')
BEGIN
    ALTER TABLE [FacturaDetalle] ADD [ImpuestoId] smallint NOT NULL DEFAULT CAST(0 AS smallint);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315184449_camposdetalle')
BEGIN
    ALTER TABLE [FacturaDetalle] ADD [ValorImpuesto] real NOT NULL DEFAULT CAST(0 AS real);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20220315184449_camposdetalle')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20220315184449_camposdetalle', N'6.0.3');
END;
GO

COMMIT;
GO

